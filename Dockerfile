FROM node:12 as builder
WORKDIR /builder
COPY ./package.json .
RUN yarn
COPY . .
RUN yarn build

FROM nginx:1.17
COPY --from=builder /builder/build /app
COPY nginx.conf /etc/nginx/conf.d/default.conf
