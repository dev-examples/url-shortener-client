# Demo app for Shortener component

## Usage of the component

Shortener component is designed to be used as a single import such as shown in App.jsx and below:

```
import Shortener from './components/Shortener'
...
<Shortener baseURL="/api" />
```

## Developing

To start front-end development live-reload server:
`yarn`
`yarn start`

Requires separately running the url-shortener-api on port 8080.

## Running unit tests

`yarn test`
