import React from 'react'
import './App.css'
import Shortener from '../Shortener'

function App() {
  return (
    <div className="App">
      <Shortener baseURL="/api" />
    </div>
  )
}

export default App
