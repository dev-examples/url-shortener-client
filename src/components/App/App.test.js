/* global test, expect */
import React from 'react'
import { render } from '@testing-library/react'
import App from '.'

test('renders app container', () => {
  const { container } = render(<App />)
  const shortener = container.querySelector('.App')
  expect(shortener).toBeInTheDocument()
})
