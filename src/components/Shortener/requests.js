import axios from 'axios'

async function requestShortURL({ baseURL, originalURL }) {
  try {
    const response = await axios.post(`${baseURL}/shorten-url`, {
      originalURL,
    })
    return response
  } catch (err) {
    if (err.response && err.response.data) {
      const { error } = err.response.data
      throw new Error(error || err.response.data)
    } else {
      throw err
    }
  }
}

export { requestShortURL }
