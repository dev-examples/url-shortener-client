import React from 'react'
import PropTypes from 'prop-types'
import './Result.scss'

function Result({ shortURL }) {
  return (
    <div className="shortener-result">
      {shortURL ? (
        <>
          <div>Short Link:</div>
          <a href={shortURL} target="_blank" rel="noopener noreferrer">
            {shortURL}
          </a>
        </>
      ) : null}
    </div>
  )
}

Result.propTypes = {
  shortURL: PropTypes.string.isRequired, // baseURL for POST shorten-url request
}

export default Result
