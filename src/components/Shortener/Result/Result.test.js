/* global test, expect */
import React from 'react'
import { render } from '@testing-library/react'
import Result from '.'

test('renders short URL', () => {
  const shortURL = 'http://server/abcd'
  const { container } = render(<Result shortURL={shortURL} />)
  const link = container.querySelector('a')
  expect(link).toBeInTheDocument()
})

test('renders does not render short URL', () => {
  const shortURL = ''
  const { container } = render(<Result shortURL={shortURL} />)
  const link = container.querySelector('a')
  expect(link).toBeNull()
})
