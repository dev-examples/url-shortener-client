import React, { useState, useEffect, useCallback } from 'react'
import PropTypes from 'prop-types'
import { requestShortURL } from './requests'
import Form from './Form'
import Result from './Result'
import './Shortener.scss'

function Shortener({ baseURL }) {
  const [originalURL, setOriginalURL] = useState('')
  const [shortURL, setShortURL] = useState('')
  const [message, setMessage] = useState('')
  const [pending, setPending] = useState(false)

  const handleShortenURL = useCallback(
    async (event) => {
      event.preventDefault()
      try {
        if (!pending) {
          setShortURL()
          setPending(true)
          const { data } = await requestShortURL({ baseURL, originalURL })
          setShortURL(data.shortURL)
          setMessage(data.warning)
        }
      } catch (e) {
        setMessage(e.message)
      } finally {
        setPending(false)
      }
    },
    [pending, originalURL, baseURL]
  )

  useEffect(() => {
    setShortURL('')
    setMessage('')
  }, [originalURL])

  return (
    <div className="shortener">
      <Form
        {...{
          message,
          pending,
          setMessage,
          originalURL,
          setOriginalURL,
          handleShortenURL,
        }}
      />
      <div className="result">
        <Result shortURL={shortURL} />
      </div>
    </div>
  )
}

Shortener.propTypes = {
  baseURL: PropTypes.string, // baseURL for POST shorten-url request
}
Shortener.defaultProps = {
  baseURL: '/api',
}

export default Shortener
