/* global test, expect, jest */
import React from 'react'
import { render } from '@testing-library/react'
import Form from '.'

const createProps = () => {
  return {
    originalURL: 'test:',
    setOriginalURL: jest.fn(),
    pending: false,
    message: '',
    setMessage: jest.fn(),
    handleShortenURL: jest.fn(),
  }
}

test('renders form', () => {
  const props = createProps()
  const { getByText, getByDisplayValue } = render(<Form {...props} />)
  expect(getByDisplayValue(/test:/, { selector: 'input' })).toBeInTheDocument()
  expect(
    getByText(/Make Short URL/, { selector: 'button' })
  ).toBeInTheDocument()
})

test('renders error message', () => {
  const props = createProps()
  props.originalURL = ''
  props.message = 'test message'
  const { getByText } = render(<Form {...props} />)
  expect(
    getByText(RegExp(props.message), { selector: 'div' })
  ).toBeInTheDocument()
})

test('renders progress indicator', () => {
  const props = createProps()
  props.pending = true
  const { container } = render(<Form {...props} />)
  expect(container.querySelector('.spinner')).toBeInTheDocument()
})
