import React from 'react'
import PropTypes from 'prop-types'
import './Form.scss'

function Shortener({
  originalURL,
  setOriginalURL,
  pending,
  message,
  setMessage,
  handleShortenURL,
}) {
  const handleOriginalURLChange = ({ target: { value } }) =>
    setOriginalURL(value)

  const handleInvalidInput = () =>
    setMessage(
      'It has to be a valid URL, no whitespace characters allowed, maximum 8192 characters'
    )

  return (
    <form className="shortener-form" onSubmit={handleShortenURL}>
      <div>URL to be shortened:</div>
      <input
        type="url"
        name="originalURL"
        defaultValue={originalURL}
        onChange={handleOriginalURLChange}
        onInvalid={handleInvalidInput}
        placeholder="Type or paste any URL here"
        disabled={pending}
        required
        pattern="\S{2,8192}"
      />
      {!pending ? <div className="error">{message}</div> : null}
      {pending ? <div className="spinner">in progress ...</div> : null}
      <button type="submit" disabled={pending}>
        Make Short URL
      </button>
    </form>
  )
}

Shortener.propTypes = {
  originalURL: PropTypes.string.isRequired,
  setOriginalURL: PropTypes.func.isRequired,
  message: PropTypes.string.isRequired,
  setMessage: PropTypes.func.isRequired,
  pending: PropTypes.bool.isRequired,
  handleShortenURL: PropTypes.func.isRequired,
}

export default Shortener
